<?php
/**
 * Created by PhpStorm.
 * User: kulekci
 * Date: 12.10.2018
 * Time: 05:12
 */

namespace ShortestPath;


class GraphBuilder
{
    public static function buildFromFile(string $file): Graph
    {
        $graph = new Graph();
        if (! \file_exists($file)) {
            throw new \RuntimeException('File not found! ['.$file.']');
        }
        foreach (new \SplFileObject($file) as $line) {
            $node = self::lineParser($line);
            if ($node) {
                $graph->addNode($node);
            }
        }

        /**
         * @var Node $node
         * @var Node $subNode
         */
        foreach ($graph->getNodes() as $key => $node) {
            foreach ($graph->getNodes() as $subNode) {
                $node->addEdge($subNode);
            }
        }

        return $graph;
    }

    private static function lineParser($line): ?Node
    {
        if (\preg_match('/([\S\ ]+) ([\+\-]*\d+\.*\d*) ([\+\-]*\d+\.\d*)/', $line, $matches)) {
            return new Node($matches[1], new Point($matches[2], $matches[3]));
        }

        return null;
    }
}
