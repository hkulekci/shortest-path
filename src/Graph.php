<?php
namespace ShortestPath;

class Graph
{
    /**
     * @var Node[]
     */
    private $nodes = [];

    public function addNode(Node $node): void
    {
        $this->nodes[] = $node;
    }

    public function getNodes(): array
    {
        return $this->nodes;
    }

    public function __toString()
    {
        $response = 'Graph : ' . PHP_EOL;
        foreach ($this->getNodes() as $node) {
            /**
             * @var Node $node
             */
            $response .= $node;
        }

        return $response;
    }
}
