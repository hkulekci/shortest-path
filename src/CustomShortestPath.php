<?php
namespace ShortestPath;

class CustomShortestPath implements ShortestPath
{
    protected $graph;

    public function __construct(Graph $graph)
    {
        $this->graph = $graph;
    }

    public function findPathAsString(): string
    {
        $result = '';
        $path = $this->findPath();
        foreach ($path as $p) {
            $result .= $p->getName();
        }

        return $result;
    }

    public function findPath(): array
    {
        $nodes = $this->graph->getNodes();
        $currentNode = array_shift($nodes);
        $visitedNodes = [];
        $shortestPath = [];
        while ($currentNode !== null) {
            /**
             * @var Node $currentNode
             */
            $shortestPath[] = $currentNode;
            $visitedNodes[] = $currentNode->getName();
            foreach ($currentNode->getEdges() as $edge) {
                /**
                 * @var Edge $edge
                 */
                if (!\in_array($edge->getNode()->getName(), $visitedNodes, true)) {
                    $currentNode = $edge->getNode();
                    break;
                }
                $currentNode = null;
            }
        }

        return $shortestPath;
    }
}
