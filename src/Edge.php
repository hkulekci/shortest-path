<?php
namespace ShortestPath;

class Edge
{
    private $target;
    private $distance;
    public function __construct(Node $target, Node $base)
    {
        $distanceCalculator = new DistanceCalculator($base->getPoint(), $target->getPoint());
        $this->distance = $distanceCalculator->calculate();
        $this->target = $target;
    }
    public function getNode(): Node
    {
        return $this->target;
    }
    public function getDistance(): float
    {
        return $this->distance;
    }
}
