<?php
namespace ShortestPath;

interface ShortestPath
{
    public function __construct(Graph $nodeStorage);

    public function findPath(): array;
}
