<?php
namespace ShortestPath;

class Node {
    private $point;
    private $name;
    private $edges = [];
    public function __construct(string $name, Point $point)
    {
        $this->point = $point;
        $this->name = $name;
    }
    public function getName(): string
    {
        return $this->name;
    }
    public function getPoint(): Point
    {
        return $this->point;
    }
    public function getEdges(): array
    {
        return $this->edges;
    }
    public function addEdge(Node $node): void
    {
        $this->edges[] = new Edge($node, $this);
        $this->sortEdgesByDistance();
    }

    private function sortEdgesByDistance(): void
    {
        usort($this->edges, function ($a, $b) {
            return $a->getDistance() > $b->getDistance();
        });
    }

    public function __toString()
    {
        $response = 'Node : ' . $this->getName() . PHP_EOL;
        foreach ($this->getEdges() as $edge) {
            /**
             * @var Edge $edge
             */
            $response .= ' - Edge ' . $edge->getNode()->getName() . ' - ' . $edge->getDistance() . PHP_EOL;
        }

        return $response;
    }
}
