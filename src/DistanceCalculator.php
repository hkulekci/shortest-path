<?php
namespace ShortestPath;

class DistanceCalculator
{
    private $point1;
    private $point2;
    public function __construct(Point $point1, Point $point2)
    {
        $this->point1 = $point1;
        $this->point2 = $point2;
    }
    public function calculate(): float
    {
        $this->point1->getLat();
        $this->point2->getLat();

        return sqrt(
            (($this->point2->getLat() - $this->point1->getLat()) ** 2) +
            (($this->point2->getLon() - $this->point1->getLon()) ** 2)
        );
    }
}
