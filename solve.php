<?php
include 'vendor/autoload.php';

use ShortestPath\NodeStorage;

$graph = \ShortestPath\GraphBuilder::buildFromFile(__DIR__ . '/cities.txt');
$algorithm = new \ShortestPath\CustomShortestPath($graph);
$path = $algorithm->findPath();
foreach ($path as $node) {
    echo $node->getName() . PHP_EOL;
}
