<?php
namespace ShortestPathTest;

use PHPUnit\Framework\TestCase;
use ShortestPath\CustomShortestPath;
use ShortestPath\Graph;
use ShortestPath\Node;
use ShortestPath\Point;

class CustomShortestPathTest extends TestCase
{
    private $graph;

    protected function setUp(): void
    {
        parent::setUp();
        $nodes = [];
        $nodes[] = new Node('A', new Point(1,1));
        $nodes[] = new Node('B', new Point(0,3));
        $nodes[] = new Node('C', new Point(-4,3));
        $nodes[] = new Node('D', new Point(-5,-5));
        $nodes[] = new Node('E', new Point(-1,-1));

        /**
         * @var Node $node
         */
        foreach ($nodes as $node) {
            foreach ($nodes as $relatedNode) {
                $node->addEdge($relatedNode);
            }
        }

        $this->graph = new Graph();
        foreach ($nodes as $node) {
            $this->graph->addNode($node);
        }
    }

    public function testCustomShortestPath(): void
    {
        $problemSolver = new CustomShortestPath($this->graph);
        $this->assertEquals('ABCED', $problemSolver->findPathAsString());
    }
}
