<?php
namespace ShortestPathTest;

use PHPUnit\Framework\TestCase;
use ShortestPath\DistanceCalculator;
use ShortestPath\Point;

class DistanceCalculatorTest extends TestCase
{
    public function testDistanceCalculation(): void
    {
        $distance = new DistanceCalculator(
            new Point(0,-9),
            new Point(12, 0)
        );
        $this->assertEquals(15, $distance->calculate());

        $distance = new DistanceCalculator(
            new Point(0, 5),
            new Point(12, 0)
        );
        $this->assertEquals(13, $distance->calculate());
    }
}
