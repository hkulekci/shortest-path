<?php
namespace ShortestPathTest;

use PHPUnit\Framework\TestCase;
use ShortestPath\GraphBuilder;

class GraphBuilderTest extends TestCase
{
    public function testFileNotFoundException(): void
    {
        $this->expectException(\RuntimeException::class);
        GraphBuilder::buildFromFile(__DIR__ . '/test_foo.txt');
    }

    public function testGraphBuilder(): void
    {
        $graph = GraphBuilder::buildFromFile(__DIR__ . '/test.txt');
        $nodes = $graph->getNodes();
        $this->assertEquals('A', $nodes[0]->getName());
        $this->assertEquals('B', $nodes[1]->getName());
        $this->assertEquals('C', $nodes[2]->getName());
        $this->assertEquals('D', $nodes[3]->getName());
        $this->assertEquals('E', $nodes[4]->getName());
    }
}
