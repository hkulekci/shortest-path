<?php
namespace ShortestPathTest;

use PHPUnit\Framework\TestCase;
use ShortestPath\Edge;
use ShortestPath\Node;
use ShortestPath\Point;

class NodeTest extends TestCase
{
    public function testNodeDistanceCalculation(): void
    {
        $x = new Node('X', new Point(0,-9));
        $y = new Node('Y', new Point(12, 0));
        $z = new Node('Z', new Point(0, 5));
        $x->addEdge($y);
        $z->addEdge($y);

        /**
         * @var Edge $XtoY
         */
        $XtoY = $x->getEdges()[0];
        $ZtoY = $z->getEdges()[0];

        $this->assertEquals(15, $XtoY->getDistance());
        $this->assertEquals(13, $ZtoY->getDistance());
    }

    public function testNodeEdgeSort(): void
    {
        $a = new Node('A', new Point(1, 1));
        $b = new Node('B', new Point(0, 3));
        $c = new Node('C', new Point(-4, 3));

        $a->addEdge($c);
        $a->addEdge($b);

        $firstEdge = $a->getEdges()[0];
        $secondEdge = $a->getEdges()[1];

        $this->assertEquals('B', $firstEdge->getNode()->getName());
        $this->assertEquals('C', $secondEdge->getNode()->getName());
    }
}
